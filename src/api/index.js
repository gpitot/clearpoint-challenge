import Data from "../data/sample-data.json";

/**
 * Gets company information.
 * @return {Object} returns company info object, companyName, companyMotto, companyEst
 */
const getCompany = async () => {
  return new Promise((resolve, reject) => {
    //fake api request time
    setTimeout(() => {
      resolve(Data.companyInfo);
    }, Math.random() * 1000);
  });
};

/**
 * Gets basic information of employees.
 * @param {number} offset offset the employee list by this amount, default = 0;
 * @param {number} limit limit of employees to return, default = 20;
 * @return {Array} list of employees, with only basic info in objects
 */
const getEmployees = async (offset = 0, limit = 20) => {
  return new Promise((resolve, reject) => {
    //fake api request time
    //get correct data
    const { employees } = Data;
    const filtered = employees.map(
      ({ id, avatar, firstName, lastName, bio }) => ({
        id,
        avatar,
        firstName,
        lastName,
        bio: bio.substring(0, 75)
      })
    );
    setTimeout(() => {
      resolve(filtered);
    }, Math.random() * 1000);
  });
};

/**
 * Gets employee full info.
 * @param {uuid} id An employee's id.
 * @return {object} Object containing id, avatar, firstName, lastName, jobTitle, bio, age, dateJoined OR NULL if employee doesnt exist
 */
const getEmployee = async id => {
  return new Promise((resolve, reject) => {
    //fake api request time
    //get correct data
    const { employees } = Data;
    const filtered = employees.filter(employee => employee.id === id);
    if (filtered.length < 1) {
        return reject(null);
    }
    setTimeout(() => {
      resolve(filtered[0]);
    }, Math.random() * 1500);
  });
};

export { getCompany, getEmployee, getEmployees };

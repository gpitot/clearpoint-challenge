import React, { useState } from "react";
import styled from "styled-components";

const sortEmployees = (employees, key) => {
  const updatedEmployees = [...employees];
  updatedEmployees.sort((a, b) => {
    if (a[key] < b[key]) return -1;
    if (a[key] > b[key]) return 1;

    return 0;
  });
  return updatedEmployees;
};

export default ({ employees , filterEmployees}) => {
  const [sortOrder, setSortOrder] = useState("firstName");

  const handleChange = e => {
    setSortOrder(e.target.value);
    filterEmployees(sortEmployees(employees, e.target.value));
  };

  return (
    <Area>
      <label htmlFor="sort">Sort by</label>
      <select value={sortOrder} onChange={handleChange} id="sort">
        <option value="firstName">firstName</option>
        <option value="lastName">lastName</option>
      </select>
    </Area>
  );
};

const Area = styled.div`
  label {
    margin-right: 10px;
  }
  select {
      border:solid 1px #222;
      padding:8px;
  }
`;

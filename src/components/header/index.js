import React, { useState, useEffect } from "react";
import styled from "styled-components";

import { getCompany } from "../../api";

export default () => {
  const [companyInfo, setCompanyInfo] = useState(null);

  useEffect(() => {
    if (companyInfo === null) {
      getCompany().then(data => {
        setCompanyInfo(data);
      });
    }
  });

  if (companyInfo === null) return null;
  const { companyName, companyMotto, companyEst } = companyInfo;
  return (
    <Header>
      <Inner>
        <h1>{companyName}</h1>
        <Info>
          <div>{companyMotto}</div>
          <div>Since {companyEst}</div>
        </Info>
      </Inner>
    </Header>
  );
};

const Header = styled.header`
  background: #c2d1ff;
  h1 {
    margin: 0;
  }
`;

const Inner = styled.div`
  padding: 12px 30px;
  max-width: 1500px;
  margin: auto;
`;

const Info = styled.div`
  display: flex;
  justify-content: space-between;


  @media (max-width:600px) {
      flex-direction:column;
  }
`;

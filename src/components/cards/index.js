import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { getEmployees } from "../../api";

import Card from "./card";
import Modal from "./modal";

import Sorting from "../sorting";
import Filter from "../filter";

export default () => {
  const [employees, setEmployees] = useState(null);

  const [modalEmployee, setModalEmployee] = useState(null);

  useEffect(() => {
    if (employees === null) {
      getEmployees(0, 20).then(data => {
        //set loaded to false for every employee
        const employeesWithLoaded = data.map(emp => ({
          ...emp,
          loaded: false
        }));
        setEmployees(employeesWithLoaded);
      });
    }
  });

  const clickCard = employee => {
    setModalEmployee(employee);
  };

  const clearModal = () => {
    setModalEmployee(null);
  };

  const updateEmployee = employee => {
    employee.loaded = true;
    //update employee in employees
    const updatedEmployees = [...employees];
    for (let i = 0; i < updatedEmployees.length; i += 1) {
      if (updatedEmployees[i].id === employee.id) {
        updatedEmployees[i] = { ...employee };
        setModalEmployee({ ...employee });
        break;
      }
    }
    setEmployees(updatedEmployees);
  };

  const filterEmployees = visible => {
    setEmployees(visible);
  };

  //dont return until employees are loaded
  if (employees === null) return null;

  return (
    <React.Fragment>
      <TitleArea>
        <h1>Our Employees</h1>
        <SortArea>
          <Sorting employees={employees} filterEmployees={filterEmployees} />
          <Filter filterEmployees={filterEmployees} employees={employees} />
        </SortArea>
      </TitleArea>
      <Area>
        {employees.map(employee => {
          //getting active state of card
          const isActive =
            modalEmployee !== null ? employee.id === modalEmployee.id : false;

          if (employee.visible === false) return null;
          return (
            <Card
              key={employee.id}
              employee={employee}
              handleClick={clickCard}
              isActive={isActive}
            />
          );
        })}
      </Area>
      {modalEmployee && (
        <Modal
          employee={modalEmployee}
          handleClick={clearModal}
          updateEmployee={updateEmployee}
        />
      )}
    </React.Fragment>
  );
};

const Area = styled.div`
  max-width: 1500px;
  width: 100%;

  margin: 50px auto;
  display: flex;
  flex-flow: row wrap;
  justify-content: center;
`;

const TitleArea = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  max-width: 1500px;
  width: 100%;
  margin: 50px auto;
  padding: 12px 30px;
  border-bottom: solid 2px #222;


  @media (max-width:600px) {
      flex-direction:column;
      margin:20px 0;
  }
`;


const SortArea = styled.div`
    display:flex;
    align-items:center;
`;
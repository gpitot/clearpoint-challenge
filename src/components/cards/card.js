import React from "react";
import styled from "styled-components";

export default props => {
  const {
    employee: { avatar, firstName, lastName, bio },
    handleClick,
    isActive
  } = props;

  return (
    <Card onClick={()=> handleClick(props.employee)} isActive={isActive}>
      <Photo>
        <img src={avatar} />
      </Photo>
      <Profile>
        <Name>
          {firstName} {lastName}
        </Name>
        <Bio>{bio.substring(0, 75)}</Bio>
      </Profile>
    </Card>
  );
};

const Photo = styled.div`
  flex-grow: 0;
  flex-shrink: 0;
  flex-basis: 128px;
  overflow: hidden;

  img {
    display: block;
    transform: scale(1);
    transition: transform 300ms ease;
  }
`;

const Card = styled.div`
  display: flex;
  width: 400px;
  height: 128px;
  border-radius: 5px;
  border: solid 1px #222;
  margin: 10px;
  overflow: hidden;
  background : ${props => props.isActive ? 'lightblue' : 'white'};
  cursor: pointer;
  box-shadow: 0 5px 10px rgba(154, 160, 185, 0.5),
    0 15px 40px rgba(166, 173, 201, 0.2);
  &:hover ${Photo} img {
    transform: scale(1.05);
  }
`;

const Profile = styled.div`
  padding: 0 10px;
`;

const Name = styled.h3``;

const Bio = styled.p``;

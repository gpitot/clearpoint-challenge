import React, { useEffect } from "react";
import styled, { keyframes } from "styled-components";
import { getEmployee } from "../../api";

export default ({ employee, handleClick, updateEmployee }) => {
  const {
    id,
    avatar,
    firstName,
    lastName,
    jobTitle,
    bio,
    age,
    dateJoined,
    loaded
  } = employee;

  useEffect(() => {
    let isRequesting = true;

    if (loaded === false) {
      getEmployee(id).then(data => {
        if (data === null) {
          //throw error
        } else {
          if (isRequesting) {
            //if isRequesting (i.e. component is still active) then updateEmployee
            updateEmployee(data);
          }
        }
      });
    }
    //on unmount set isRequesting to false so that we dont use stale data from api request
    return () => (isRequesting = false);
  });

  const stopClick = e => {
    //clicking on modal directly doesnt exit it
    e.stopPropagation();
  };

  return (
    <ModalOuter onClick={handleClick}>
      <Modal onClick={stopClick}>
        <ExitBtn onClick={handleClick}>X</ExitBtn>
        <Profile>
          <img src={avatar} />

          {loaded && (
            <React.Fragment>
              <h4>
                Title : <span>{jobTitle}</span>
              </h4>
              <h5>Age : {age}</h5>
              <h5>Joined : {dateJoined.substring(0, 10)}</h5>
            </React.Fragment>
          )}
        </Profile>
        <Bio>
          <h2>
            {firstName} {lastName}
          </h2>
          <hr />
          <p>{bio}</p>
        </Bio>
      </Modal>
    </ModalOuter>
  );
};

const ModalOuter = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100vh;
  background: rgba(0, 0, 0, 0.6);
`;

const modalPopup = keyframes`
    from {
        opacity:0;
        transform:translate(-50%, -50%) scale(0.8);
    }

    to {
        opacity:1;
        transform:translate(-50%, -50%) scale(1);
    }
`;

const Modal = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%) scale(0.8);
  width: 700px;
  max-width: 100%;
  min-height: 300px;
  background: white;
  padding: 30px;
  opacity: 0;

  animation-name: ${modalPopup};
  animation-duration: 300ms;
  animation-timing-function: easing;
  animation-fill-mode: forwards;

  display: flex;
  border-radius: 8px;

  @media (max-width: 600px) {
    flex-direction: column;
  }
`;

const Profile = styled.div`
  flex-basis: 168px;
  flex-shrink: 0;
  display: flex;
  margin-right: 12px;
  flex-direction: column;
  border-right: solid 2px #222;

  @media (max-width: 600px) {
    flex-grow: 1;
    align-items: center;
    border: none;
  }

  h4 {
    margin: 0;
    font-size: 14px;
    span {
      font-style: italic;
    }
  }

  h5 {
    margin: 0;
  }

  img {
    align-self: flex-start;
    margin-bottom: 12px;

    @media (max-width: 600px) {
        align-self:center;
    }
  }
`;

const Bio = styled.div`
  flex-grow: 1;
  margin-left: 12px;
`;

const ExitBtn = styled.button`
  position: absolute;
  top: -50px;
  right: 0;

  color: red;
  font-size: 20px;
  font-weight: 800;
  background: white;
  border-radius: 8px;
  padding: 8px 14px;
  line-height: 25px;
  cursor: pointer;
  border: none;

  transition: all 200ms ease;

  &:hover {
    background: #222;
  }
`;

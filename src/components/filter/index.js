import React, { useState } from "react";
import styled from "styled-components";

const filter = (query, employees) => {
  const realQuery = query.toLowerCase().trim();
  //immutable

  const realEmployees = [...employees];
  for (let i = 0; i < realEmployees.length; i += 1) {
    let visible = false;
    if (
      realEmployees[i].firstName.toLowerCase().includes(realQuery) ||
      realEmployees[i].lastName.toLowerCase().includes(realQuery)
    ) {
      visible = true;
    }
    realEmployees[i] = {
      ...realEmployees[i],
      visible
    };
  }
  return realEmployees;
};

export default ({ filterEmployees, employees }) => {
  const [query, setQuery] = useState("");

  const handleChange = e => {
    setQuery(e.target.value);
    filterEmployees(filter(e.target.value, employees));
  };

  return (
    <Area>
      <label htmlFor="filter">Search</label>
      <input
        id="filter"
        type="text"
        placeholder="Search"
        value={query}
        onChange={handleChange}
      />
    </Area>
  );
};

const Area = styled.div`
    margin-left:15px;
    label {
        margin-right:10px;
    }

    input {
        padding:12px;
    }
`;

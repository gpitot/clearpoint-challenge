import React from "react";
import Header from "./components/header";
import Cards from "./components/cards";

export default () => (
  <React.Fragment>
    <Header />
    <Cards />
  </React.Fragment>
);
